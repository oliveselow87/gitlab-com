local stages = {
  stages: [
    'check',
    'dryrun-check',
    'dryrun',
    'non-prod-cny:deploy',
    'non-prod:deploy',
    'non-prod:QA',
    'gprd-cny:deploy',
    'gprd:deploy:alpha',
    'gprd:deploy:beta',
    'cleanup',
    'scheduled',
  ],
};

local includes = {
  include: [
    // Dependency scanning
    // https://docs.gitlab.com/ee/user/application_security/dependency_scanning/
    { template: 'Security/Dependency-Scanning.gitlab-ci.yml' },
  ],
};

local exceptCom = {
  'if': '$CI_API_V4_URL == "https://gitlab.com/api/v4"',
  when: 'never',
};

local exceptOps = {
  'if': '$CI_API_V4_URL == "https://ops.gitlab.net/api/v4"',
  when: 'never',
};

local idTokens = {
  id_tokens: {
    VAULT_ID_TOKEN: {
      aud: 'https://vault.gitlab.net',
    },
  },
};

local vaultRw = {
  variables+: {
    HELMFILE_VAULT_AUTH_ROLE: 'gitlab-com_gl-infra_k8s-workloads_gitlab-com-rw',
    VAULT_KUBERNETES_ROLE: 'cluster-admin',
  },
};

local image = '${CI_REGISTRY}/gitlab-com/gl-infra/k8s-workloads/common/k8-helm-ci:${CI_IMAGE_VERSION}';

local variables = {
  variables: {
    // renovate: datasource=docker depName=registry.gitlab.com/gitlab-com/gl-infra/k8s-workloads/common/k8-helm-ci versioning=docker
    CI_IMAGE_VERSION: 'v17.42.0',
    AUTO_DEPLOY: 'false',
    HELMFILE_VAULT_AUTH_ROLE: '${VAULT_AUTH_ROLE}',
    VAULT_GCP_IMPERSONATED_ACCOUNT: '${PROJECT}--k8s-workloads-ro',
    VAULT_KUBERNETES_ROLE: 'cluster-view',
  },
};

local workflow_rules = {
  workflow: {
    name: '$GITLAB_COM_PIPELINE_NAME',
    rules: [
      {
        // Give a unique name for all Auto-Deployments
        'if': '$AUTO_DEPLOY == "true" && $GITLAB_IMAGE_TAG',
        variables: {
          // These values come from Deployer
          GITLAB_COM_PIPELINE_NAME: 'auto-deploy: $ENVIRONMENT - $GITLAB_IMAGE_TAG',
        },
      },
      {
        // Give a unique name for downstream QA jobs
        'if': '$AUTO_DEPLOY == "false"',
        variables: {
          QA_PIPELINE_NAME: '$CI_PROJECT_PATH: $ENVIRONMENT',
        },
      },
      // Make sure we do branch pipelines only (no duplicate pipelines)
      // https://docs.gitlab.com/ee/ci/yaml/workflow.html#workflowrules-templates
      //{ template: 'Workflows/Branch-Pipelines.gitlab-ci.yml' },
      {
        'if': '$CI_COMMIT_TAG',
      },
      {
        'if': '$CI_COMMIT_BRANCH',
      },
    ],
  },
};

local mainRegion = 'us-east1';

local clusterAttrs = {
  pre: {
    ENVIRONMENT_WITHOUT_STAGE: 'pre',
    GOOGLE_PROJECT: 'gitlab-pre',
    GKE_CLUSTER: 'pre-gitlab-gke',
    GOOGLE_REGION: mainRegion,
  },
  gstg: {
    ENVIRONMENT_WITHOUT_STAGE: 'gstg',
    GOOGLE_PROJECT: 'gitlab-staging-1',
    GKE_CLUSTER: 'gstg-gitlab-gke',
    GOOGLE_REGION: mainRegion,
  },
  'gstg-us-east1-b': {
    GOOGLE_PROJECT: 'gitlab-staging-1',
    GKE_CLUSTER: 'gstg-us-east1-b',
    GOOGLE_ZONE: 'us-east1-b',
  },
  'gstg-us-east1-c': {
    GOOGLE_PROJECT: 'gitlab-staging-1',
    GKE_CLUSTER: 'gstg-us-east1-c',
    GOOGLE_ZONE: 'us-east1-c',
  },
  'gstg-us-east1-d': {
    GOOGLE_PROJECT: 'gitlab-staging-1',
    GKE_CLUSTER: 'gstg-us-east1-d',
    GOOGLE_ZONE: 'us-east1-d',
  },
  gprd: {
    ENVIRONMENT_WITHOUT_STAGE: 'gprd',
    GOOGLE_PROJECT: 'gitlab-production',
    GKE_CLUSTER: 'gprd-gitlab-gke',
    GOOGLE_REGION: mainRegion,
  },
  'gprd-us-east1-b': {
    GOOGLE_PROJECT: 'gitlab-production',
    GKE_CLUSTER: 'gprd-us-east1-b',
    GOOGLE_ZONE: 'us-east1-b',
  },
  'gprd-us-east1-c': {
    GOOGLE_PROJECT: 'gitlab-production',
    GKE_CLUSTER: 'gprd-us-east1-c',
    GOOGLE_ZONE: 'us-east1-c',
  },
  'gprd-us-east1-d': {
    GOOGLE_PROJECT: 'gitlab-production',
    GKE_CLUSTER: 'gprd-us-east1-d',
    GOOGLE_ZONE: 'us-east1-d',
  },
};

local baseCiConfigs = {
  '.pre-base': {
    variables: {
      PROJECT: clusterAttrs.pre.GOOGLE_PROJECT,
      REGION: clusterAttrs.pre.GOOGLE_REGION,
      ENVIRONMENT_WITHOUT_STAGE: clusterAttrs.pre.ENVIRONMENT_WITHOUT_STAGE,
    },
    environment: {
      name: 'pre',
      url: 'https://pre.gitlab.com',
    },
  },
  '.pre': {
    extends: [
      '.pre-base',
    ],
    variables: {
      CLUSTER: clusterAttrs.pre.GKE_CLUSTER,
    },
    environment: {
      name: 'pre',
    },
    resource_group: 'pre',
  },
  '.gstg-base': {
    variables: {
      PROJECT: clusterAttrs.gstg.GOOGLE_PROJECT,
      ENVIRONMENT_WITHOUT_STAGE: clusterAttrs.gstg.ENVIRONMENT_WITHOUT_STAGE,
    },
    environment: {
      url: 'https://staging.gitlab.com',
    },
  },
  '.gstg': {
    extends: [
      '.gstg-base',
    ],
    variables: {
      CLUSTER: clusterAttrs.gstg.GKE_CLUSTER,
      REGION: clusterAttrs.gstg.GOOGLE_REGION,
    },
    environment: {
      name: 'gstg',
    },
    resource_group: 'gstg',
  },
  '.gstg-us-east1-b': {
    extends: [
      '.gstg-base',
    ],
    variables: {
      CLUSTER: clusterAttrs['gstg-us-east1-b'].GKE_CLUSTER,
      REGION: clusterAttrs['gstg-us-east1-b'].GOOGLE_ZONE,
    },
    environment: {
      name: 'gstg-us-east1-b',
    },
    resource_group: 'gstg-us-east1-b',
  },
  '.gstg-us-east1-c': {
    extends: [
      '.gstg-base',
    ],
    variables: {
      CLUSTER: clusterAttrs['gstg-us-east1-c'].GKE_CLUSTER,
      REGION: clusterAttrs['gstg-us-east1-c'].GOOGLE_ZONE,
    },
    environment: {
      name: 'gstg-us-east1-c',
    },
    resource_group: 'gstg-us-east1-c',
  },
  '.gstg-us-east1-d': {
    extends: [
      '.gstg-base',
    ],
    variables: {
      CLUSTER: clusterAttrs['gstg-us-east1-d'].GKE_CLUSTER,
      REGION: clusterAttrs['gstg-us-east1-d'].GOOGLE_ZONE,
    },
    environment: {
      name: 'gstg-us-east1-d',
    },
    resource_group: 'gstg-us-east1-d',
  },
  '.gprd-base': {
    variables: {
      PROJECT: clusterAttrs.gprd.GOOGLE_PROJECT,
      ENVIRONMENT_WITHOUT_STAGE: clusterAttrs.gprd.ENVIRONMENT_WITHOUT_STAGE,
    },
    environment: {
      url: 'https://gitlab.com',
    },
  },
  '.gprd': {
    extends: [
      '.gprd-base',
    ],
    variables: {
      CLUSTER: clusterAttrs.gprd.GKE_CLUSTER,
      REGION: clusterAttrs.gprd.GOOGLE_REGION,
    },
    environment: {
      name: 'gprd',
    },
    resource_group: 'gprd',
  },
  '.gprd-us-east1-b': {
    extends: [
      '.gprd-base',
    ],
    variables: {
      CLUSTER: clusterAttrs['gprd-us-east1-b'].GKE_CLUSTER,
      REGION: clusterAttrs['gprd-us-east1-b'].GOOGLE_ZONE,
    },
    environment: {
      name: 'gprd-us-east1-b',
    },
    resource_group: 'gprd-us-east1-b',
  },
  '.gprd-us-east1-c': {
    extends: [
      '.gprd-base',
    ],
    variables: {
      CLUSTER: clusterAttrs['gprd-us-east1-c'].GKE_CLUSTER,
      REGION: clusterAttrs['gprd-us-east1-c'].GOOGLE_ZONE,
    },
    environment: {
      name: 'gprd-us-east1-c',
    },
    resource_group: 'gprd-us-east1-c',
  },
  '.gprd-us-east1-d': {
    extends: [
      '.gprd-base',
    ],
    variables: {
      CLUSTER: clusterAttrs['gprd-us-east1-d'].GKE_CLUSTER,
      REGION: clusterAttrs['gprd-us-east1-d'].GOOGLE_ZONE,
    },
    environment: {
      name: 'gprd-us-east1-d',
    },
    resource_group: 'gprd-us-east1-d',
  },
};

local onlyAutoDeployFalseAndConfigChanges(environment, cluster) = {
  rules+: [
    {
      'if': '$AUTO_DEPLOY == "true"',
      when: 'never',
    },
    {
      'if': '$CI_PIPELINE_SOURCE == "schedule"',
      when: 'never',
    },
    {
      when: 'on_success',
      changes: [
        'vendor/charts/gitlab/%s/**/*' % environment,
        'vendor/charts/vault-secrets/**/*',
        '.gitlab-ci.yml',
        'bases/helmDefaults.yaml',
        'bases/__base.yaml',
        'bases/%s.yaml' % environment,
        'releases/gitlab/helmfile.yaml',
        'releases/**/values/%s.*' % environment,
        'releases/**/values/%s.*' % cluster,
        'releases/**/values/values*',
      ],
    },
  ],
};

local checkVendoredCharts = import 'ci/check-vendored-charts.libsonnet';
local shellcheck = import 'ci/shellcheck.libsonnet';
local versionChecks = import 'ci/version-checks.libsonnet';

local assertFormatting = {
  assert_formatting: {
    stage: 'check',
    image: image,
    script: |||
      find . -name '*.*sonnet' | xargs -n1 jsonnetfmt -i
      git diff --exit-code
    |||,
    rules: [
      exceptOps,
      { when: 'on_success' },
    ],
  },
};

local ciConfigGenerated = {
  ci_config_generated: {
    stage: 'check',
    image: image,
    script: |||
      make generate-ci-config
      git diff --exit-code || (echo "Please run 'make generate-ci-config'" >&2 && exit 1)
    |||,
  },
};

local notifyComMR = {
  notify_com_mr: {
    stage: 'check',
    image: '${CI_REGISTRY}/gitlab-com/gl-infra/k8s-workloads/common/notify-mr:${CI_IMAGE_VERSION}',
    secrets: {
      GITLAB_API_TOKEN: {
        file: false,
        vault: 'access_tokens/gitlab-com/${CI_PROJECT_PATH}/notify-mr/token@ci',
      },
    },
    script: |||
      notify-mr -s
    |||,
    allow_failure: true,
    rules: [
      {
        'if': '$EXPEDITE_DEPLOYMENT',
        when: 'never',
      },
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
      {
        'if': '$CI_PIPELINE_SOURCE == "schedule"',
        when: 'never',
      },
      exceptCom,
      {
        when: 'on_success',
      },
    ],
  } + idTokens,
};

local dependencyScanning = {
  dependency_scanning: {
    stage: 'check',
  },
};

local bundlerAuditDependencyScanning = {
  'bundler-audit-dependency_scanning': {
    stage: 'check',
  },
};

local retireJsDependencyScanning = {
  'retire-js-dependency_scanning': {
    stage: 'check',
  },
};

local clusterInitBeforeScript = {
  before_script: [
    |||
      if [[ "${LOG_LEVEL}" == "debug" ]]; then
        echo "A high debug level may expose secrets, this job will now exit..."
        exit 1
      fi
    |||,
    'export VAULT_TOKEN="$(vault write -field=token "auth/${VAULT_AUTH_PATH}/login" role="${HELMFILE_VAULT_AUTH_ROLE}" jwt="${VAULT_ID_TOKEN}")"',
    'export CLOUDSDK_AUTH_ACCESS_TOKEN="$(vault read -field=token "gcp/impersonated-account/${VAULT_GCP_IMPERSONATED_ACCOUNT}/token")"',
    'gcloud container clusters get-credentials "${CLUSTER}" --project "${PROJECT}" --location "${REGION}"',
    'export HELM_KUBETOKEN="$(vault write -field=service_account_token "kubernetes/${CLUSTER}/creds/${VAULT_KUBERNETES_ROLE}" kubernetes_namespace=vault-k8s-secrets cluster_role_binding=true ttl=130m)"',
  ],
};

local deploy(environment, stage, cluster, ciStage) = {
  local isCanary = stage == 'cny',
  local envPrefix = std.strReplace(environment, '-cny', ''),
  ['%s:dryrun:auto-deploy' % cluster]: {
    stage: 'dryrun',
    extends: [
      '.%s' % (if isCanary then envPrefix else cluster),
    ],
    image: image,
    script: |||
      bin/k-ctl -D %s upgrade
    ||| % if isCanary then '-s cny' else '',
    rules: [
      exceptCom,
      {
        'if': '$ENVIRONMENT == "%s" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"' % environment,
        when: 'on_success',
      },
    ],
    tags: [
      'k8s-workloads',
    ],
    [if isCanary then 'resource_group']: environment,
  } + clusterInitBeforeScript + idTokens,
  ['%s:auto-deploy' % cluster]: {
    stage: ciStage,
    extends: [
      '.%s' % (if isCanary then envPrefix else cluster),
    ],
    image: image,
    script: |||
      bin/k-ctl %s upgrade
    ||| % if isCanary then '-s cny' else '',
    rules: [
      exceptCom,
      {
        'if': '$ENVIRONMENT == "%s" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"' % environment,
        when: 'on_success',
      },
    ],
    tags: [
      'k8s-workloads',
    ],
    [if isCanary then 'resource_group']: environment,
  } + clusterInitBeforeScript + idTokens + vaultRw,
  ['%s:dryrun' % cluster]: {
    stage: 'dryrun',
    extends: [
      '.%s' % (if isCanary then envPrefix else cluster),
    ],
    secrets: {
      GITLAB_API_TOKEN: {
        file: false,
        vault: 'access_tokens/gitlab-com/${CI_PROJECT_PATH}/notify-mr/token@ci',
      },
    },
    image: image,
    script: |||
      bin/k-ctl -D %s upgrade
    ||| % if isCanary then '-s cny' else '',
    tags: [
      'k8s-workloads',
    ],
    rules: [
      exceptCom,
    ],
    [if isCanary then 'resource_group']: environment,
  } + clusterInitBeforeScript + idTokens + onlyAutoDeployFalseAndConfigChanges(envPrefix, cluster),
  ['%s:upgrade' % cluster]: {
    stage: ciStage,
    extends: [
      '.%s' % (if isCanary then envPrefix else cluster),
    ],
    variables: {
      DRY_RUN: 'false',
    },
    image: image,
    script: |||
      bin/grafana-annotate -e $CI_ENVIRONMENT_NAME
      bin/k-ctl %s upgrade
    ||| % if isCanary then '-s cny' else '',
    rules: [
      {
        'if': '$CI_DEFAULT_BRANCH != $CI_COMMIT_REF_NAME',
        when: 'never',
      },
      exceptCom,
    ],
    tags: [
      'k8s-workloads',
    ],
    [if isCanary then 'resource_group']: environment,
  } + clusterInitBeforeScript + idTokens + vaultRw + onlyAutoDeployFalseAndConfigChanges(envPrefix, cluster),
  ['%s:check-label-taxonomy' % cluster]: {
    stage: 'check',
    extends: [
      '.%s' % (if isCanary then envPrefix else cluster),
    ],
    image: image,
    script: |||
      bin/k-ctl %s template
      ./bin/check-label-taxonomy.sh
    ||| % if isCanary then '-s cny' else '',
    variables: {
      GITLAB_IMAGE_TAG: '15.test',
      SKIP_REGISTRY_MIGRATION: 'true',
    },
    rules: [
      exceptOps,
    ],
  } + onlyAutoDeployFalseAndConfigChanges(envPrefix, cluster),
};

local qaJob(name, project, allow_failure=false) = {
  ['%s:qa' % name]: {
    stage: 'non-prod:QA',
    [if allow_failure then 'allow_failure']: allow_failure,
    trigger: {
      project: project,
      strategy: 'depend',
      forward: {
        yaml_variables: true,
      },
    },
    variables: {
      SMOKE_ONLY: 'true',
      QA_PIPELINE_NAME: '$QA_PIPELINE_NAME',
    },
    rules: [
      exceptCom,
      {
        'if': '$CI_DEFAULT_BRANCH != $CI_COMMIT_REF_NAME',
        when: 'never',
      },
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
      {
        'if': '$EXPEDITE_DEPLOYMENT',
        when: 'never',
      },
      {
        'if': '$SKIP_QA == "true"',
        when: 'never',
      },
      {
        'if': '$CI_PIPELINE_SOURCE == "schedule"',
        when: 'never',
      },
      {
        when: 'on_success',
        changes: [
          'vendor/charts/gitlab/%s/**/*' % name,
          'vendor/charts/gitlab-runner/%s/**/*' % name,
          'vendor/charts/vault-secrets/**/*',
          '.gitlab-ci.yml',
          '*.yaml',
          '*.yml',
          'bases/helmDefaults.yaml',
          'bases/__base.yaml',
          'bases/%s.yaml' % name,
          'bin/**/*',
          'releases/gitlab/helmfile.yaml',
          'releases/gitlab/values/values*',
          'releases/gitlab/values/%s*' % name,
        ],
      },
    ],
  },
};

local openChartBumpMR(name) = {
  ['open-chart-bump-mr-%s' % name]: {
    stage: 'scheduled',
    image: image,
    secrets: {
      GITLAB_API_TOKEN: {
        file: false,
        vault: 'access_tokens/gitlab-com/${CI_PROJECT_PATH}/chart-bumper/token@ci',
      },
      REGISTRY_OPS_GL_USERNAME: {
        file: false,
        vault: 'ops-gitlab-net/charts/helm-ci-ro/user@shared',
      },
      REGISTRY_OPS_GL_PASSWORD: {
        file: false,
        vault: 'ops-gitlab-net/charts/helm-ci-ro/token@shared',
      },
      SSH_PRIVATE_KEY: {
        file: true,
        vault: '${VAULT_SECRETS_PATH}/shared/ssh/private_key@ci',
      },
      SSH_KNOWN_HOSTS: {
        file: true,
        vault: '${VAULT_SECRETS_PATH}/shared/ssh/known_hosts@ci',
      },
    },
    script: |||
      chmod 0400 "${SSH_PRIVATE_KEY}"
      export GIT_SSH_COMMAND="ssh -i ${SSH_PRIVATE_KEY} -o IdentitiesOnly=yes -o GlobalKnownHostsFile=${SSH_KNOWN_HOSTS}"
      git config --global user.email "ops@ops.gitlab.net"
      git config --global user.name "ops-gitlab-net"
      git remote set-url origin https://chart-bumper:${GITLAB_API_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git
      mkdir -p ~/.docker; touch ~/.docker/config.json; chmod 0600 ~/.docker/config.json
      echo -n "${REGISTRY_OPS_GL_USERNAME}:${REGISTRY_OPS_GL_PASSWORD}" \
        | jq -R '{"auths": {"registry.ops.gitlab.net": {"auth": .|@base64}}}' > ~/.docker/config.json
      echo "${GITLAB_API_TOKEN}" | glab auth login --hostname gitlab.com --stdin
      glab config set git_protocol https
      glab auth status
      ./bin/autobump-gitlab-chart.sh %s
    ||| % name,
    rules: [
      {
        'if': '$CI_PIPELINE_SOURCE == "schedule"',
      },
      {
        when: 'never',
      },
    ],
  } + idTokens,
};

local removeExpediteVariable = {
  'remove-expedite-variable': {
    stage: 'cleanup',
    secrets: {
      OPS_API_TOKEN: {
        file: false,
        vault: 'access_tokens/${VAULT_SECRETS_PATH}/remove-expedite-variable/token@ci',
      },
    },
    image: image,
    script: |||
      curl --fail --header "PRIVATE-TOKEN: ${OPS_API_TOKEN}" -X DELETE "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/variables/EXPEDITE_DEPLOYMENT"
    |||,
    rules: [
      {
        'if': '$CI_API_V4_URL == "https://gitlab.com/api/v4"',
        when: 'never',
      },
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
      {
        'if': '$CI_PIPELINE_SOURCE == "schedule"',
        when: 'never',
      },
      {
        'if': '($CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH) && $EXPEDITE_DEPLOYMENT',
      },
    ],
  } + idTokens,
};

local gitlabCIConf =
  stages
  + shellcheck
  + versionChecks
  + checkVendoredCharts
  + variables
  + workflow_rules
  + includes
  + notifyComMR
  + baseCiConfigs
  + dependencyScanning
  + bundlerAuditDependencyScanning
  + retireJsDependencyScanning
  + assertFormatting
  + ciConfigGenerated
  + deploy('pre', 'main', 'pre', 'non-prod:deploy')
  + deploy('gstg-cny', 'cny', 'gstg-cny', 'non-prod-cny:deploy')
  + deploy('gstg', 'main', 'gstg', 'non-prod:deploy')
  + deploy('gstg', 'main', 'gstg-us-east1-b', 'non-prod:deploy')
  + deploy('gstg', 'main', 'gstg-us-east1-c', 'non-prod:deploy')
  + deploy('gstg', 'main', 'gstg-us-east1-d', 'non-prod:deploy')
  + deploy('gprd-cny', 'cny', 'gprd-cny', 'gprd-cny:deploy')
  + deploy('gprd', 'main', 'gprd', 'gprd:deploy:alpha')
  + deploy('gprd', 'main', 'gprd-us-east1-b', 'gprd:deploy:alpha')
  + deploy('gprd', 'main', 'gprd-us-east1-c', 'gprd:deploy:beta')
  + deploy('gprd', 'main', 'gprd-us-east1-d', 'gprd:deploy:beta')
  + qaJob('pre', 'gitlab-org/quality/preprod', allow_failure=true)
  + qaJob('gstg', 'gitlab-org/quality/staging')
  + openChartBumpMR('pre')
  + openChartBumpMR('gstg')
  + openChartBumpMR('gprd')
  + removeExpediteVariable;

std.manifestYamlDoc(gitlabCIConf)
