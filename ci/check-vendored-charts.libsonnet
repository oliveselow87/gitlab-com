{
  'check-vendored-charts': {
    image: '${CI_REGISTRY}/gitlab-com/gl-infra/k8s-workloads/common/k8-helm-ci:${CI_IMAGE_VERSION}',
    stage: 'check',
    id_tokens: {
      VAULT_ID_TOKEN: {
        aud: 'https://vault.gitlab.net',
      },
    },
    secrets: {
      REGISTRY_OPS_GL_USERNAME: {
        file: false,
        vault: 'ops-gitlab-net/charts/helm-ci-ro/user@shared',
      },
      REGISTRY_OPS_GL_PASSWORD: {
        file: false,
        vault: 'ops-gitlab-net/charts/helm-ci-ro/token@shared',
      },
      SSH_PRIVATE_KEY: {
        file: true,
        vault: '${VAULT_SECRETS_PATH}/shared/ssh/private_key@ci',
      },
      SSH_KNOWN_HOSTS: {
        file: true,
        vault: '${VAULT_SECRETS_PATH}/shared/ssh/known_hosts@ci',
      },
    },
    script: |||
      chmod 0400 "${SSH_PRIVATE_KEY}"
      export GIT_SSH_COMMAND="ssh -i ${SSH_PRIVATE_KEY} -o IdentitiesOnly=yes -o GlobalKnownHostsFile=${SSH_KNOWN_HOSTS}"
      mkdir -p ~/.docker; touch ~/.docker/config.json; chmod 0600 ~/.docker/config.json
      echo -n "${REGISTRY_OPS_GL_USERNAME}:${REGISTRY_OPS_GL_PASSWORD}" \
        | jq -R '{"auths": {"registry.ops.gitlab.net": {"auth": .|@base64}}}' > ~/.docker/config.json
      vendir sync
      for i in pre gstg gprd; do pushd vendor/charts/gitlab/$i; helm dep update; popd; done
      git diff --exit-code vendor || (echo "Charts in this commit are not the same as upstream" >&2 && exit 1)
    |||,
    rules: [
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
      {
        'if': '$CI_COMMIT_REF_NAME == "master"',
        when: 'never',
      },
      {
        'if': '$CI_PIPELINE_SOURCE == "schedule"',
        when: 'never',
      },
      {
        'if': '$CI_API_V4_URL == "https://ops.gitlab.net/api/v4"',
        when: 'never',
      },
      {
        when: 'on_success',
      },
    ],
  },
}
