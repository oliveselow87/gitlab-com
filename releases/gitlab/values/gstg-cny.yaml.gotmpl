---

# Please note that `gstg.yaml.gotmpl` is included first for the `gstg-cny` environment,
# this file is for `cny` specific overrides

nginx-ingress:
  common:
    labels:
      stage: cny
  controller:
    autoscaling:
      minReplicas: 3

global:
  hosts:
    # gcloud compute address nginx-gke-gstg-cny
    externalIP: 10.224.34.202
  pages:
    enabled: true
    externalHttp:
      # gcloud compute address pages-gke-gstg-cny
      - 10.224.34.56
    externalHttps:
      # gcloud compute address pages-gke-gstg-cny
      - 10.224.34.56

gitlab:
  gitlab-pages:
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-cny-gitlab-pages@{{ .Values.google_project }}.iam.gserviceaccount.com
  gitlab-shell:
    workhorse:
      serviceName: webservice-internal-api
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-cny-gitlab-shell@{{ .Values.google_project }}.iam.gserviceaccount.com
    service:
      # gcloud compute address ssh-gke-gstg-cny
      loadBalancerIP: 10.224.34.124
  mailroom:
    enabled: false

  sidekiq:
    enabled: false
  webservice:
    common:
      labels:
        shard: default
        stage: cny
        tier: sv
    deployments:
      api:
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-api
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"api\", \"stage\": \"cny\"}"
        service:
          # gcloud compute address api-gke-gstg-cny
          loadBalancerIP: 10.224.34.72
      git:
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-git
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"git\", \"stage\": \"cny\"}"
        service:
          # gcloud compute address git-https-gke-gstg-cny
          loadBalancerIP: 10.224.34.201
      internal-api:
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"internal-api\", \"stage\": \"cny\"}"
      web:
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"web\", \"stage\": \"cny\"}"
          CANARY: "true"
          MALLOC_CONF: "narenas:2"
        service:
          # gcloud compute address web-gke-gstg-cny
          loadBalancerIP: 10.224.34.199
      websockets:
        hpa:
          minReplicas: 1
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-websockets
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"websockets\", \"stage\": \"cny\"}"
          GODEBUG: madvdontneed=1
        service:
          # gcloud compute address websockets-gke-gstg-cny
          loadBalancerIP: 10.224.34.200
    minReplicas: 2
    extraEnv:
      GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"git\", \"stage\": \"cny\"}"
      DISABLE_PUMA_NAKAYOSHI_FORK: "true"
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-cny-webservice@{{ .Values.google_project }}.iam.gserviceaccount.com
  kas:
    service:
      # gcloud compute address kas-internal-gke-gstg-cny
      loadBalancerIP: 10.224.34.10

registry:
  hpa:
    minReplicas: 2
  service:
    # gcloud compute address registry-gke-gstg-cny
    loadBalancerIP: 10.224.34.100
  serviceAccount:
    annotations:
      iam.gke.io/gcp-service-account: gitlab-cny-registry@{{ .Values.google_project }}.iam.gserviceaccount.com

gitlab-zoekt:
  install: false
